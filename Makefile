# This is a very rudimentary Makefile, and probably deserves a rewrite if this project gets any bigger.

TARGET_FLAG = -o

LDMALLOC_SOURCES   = ldmalloc.cpp ldmalloc_logging.cpp
LDMALLOC_TARGET    = ldmalloc.so
LDMALLOC_CXXFLAGS  = -std=c++1z -O3 -g -Wall -Wextra -pedantic
LDMALLOC_LDFLAGS   = -fPIC -ldl -shared -Wl,-z,initfirst

all:
	$(CXX) $(LDMALLOC_CXXFLAGS) $(CXXFLAGS) $(LDMALLOC_LDFLAGS) $(LDFLAGS) $(LDMALLOC_SOURCES) $(TARGET_FLAG) $(LDMALLOC_TARGET)
