// Copyright (C) 2016 Tobias Holl
// This file is subject to the license terms in the LICENSE file
// found in the top-level directory of this distribution.


#ifndef _GNU_SOURCE
// Define _GNU_SOURCE to pull in what dlfcn.h thinks is a GNU extension
// (although RTLD_NEXT is mentioned in the POSIX standard for dlsym)
#define _GNU_SOURCE
#endif
#include <dlfcn.h>

#include <csignal>
#include <cstddef>
#include <cstdlib>
#include <cstdio>
#include <mutex>
#include <new>


// Set up macros and settings

#ifdef _GLIBCXX_USE_NOEXCEPT
#define LDMALLOC_NOEXCEPT noexcept
#else
#define LDMALLOC_NOEXCEPT
#endif

#ifndef LDMALLOC_INITIALIZATION_BUFFER
#define LDMALLOC_INITIALIZATION_BUFFER 1024
#endif


// Logging tools. You can override these by using LD_PRELOAD to load another
// library prior to ldmalloc.

// C/POSIX
void ldmalloc_log_malloc(std::size_t size, void* result);
void ldmalloc_log_calloc(std::size_t count, std::size_t size, void* result);
void ldmalloc_log_realloc(void* ptr, std::size_t size, void* result);

void ldmalloc_log_posix_memalign(std::size_t alignment, std::size_t size, void* new_ptr);
void ldmalloc_log_memalign(std::size_t alignment, std::size_t size, void* result);
void ldmalloc_log_aligned_alloc(std::size_t alignment, std::size_t size, void* result);
void ldmalloc_log_valloc(std::size_t size, void* result);
void ldmalloc_log_pvalloc(std::size_t size, void* result);

void ldmalloc_log_free(void* ptr);

// C++
void ldmalloc_log_new(std::size_t size, void* result);
void ldmalloc_log_new_array(std::size_t size, void* result);
void ldmalloc_log_new_nothrow(std::size_t size, void* result);
void ldmalloc_log_new_nothrow_array(std::size_t size, void* result);

void ldmalloc_log_delete(void* ptr);
void ldmalloc_log_delete_array(void* ptr);
void ldmalloc_log_delete_nothrow(void* ptr);
void ldmalloc_log_delete_nothrow_array(void* ptr);
void ldmalloc_log_sized_delete(void* ptr, std::size_t size);
void ldmalloc_log_sized_delete_array(void* ptr, std::size_t size);

// Other logging
void ldmalloc_log_init();
void ldmalloc_log_thread_init();
void ldmalloc_log_finalize();


// Base versions of the internal allocation functions

void* (*ldmalloc_internal_malloc)(std::size_t size);
void* (*ldmalloc_internal_calloc)(std::size_t count, std::size_t size);
void* (*ldmalloc_internal_realloc)(void* ptr, std::size_t size);

int   (*ldmalloc_internal_posix_memalign)(void** memptr, std::size_t alignment, std::size_t size);
void* (*ldmalloc_internal_memalign)(std::size_t alignment, std::size_t size);
void* (*ldmalloc_internal_aligned_alloc)(std::size_t alignment, std::size_t size);
void* (*ldmalloc_internal_valloc)(std::size_t size);
void* (*ldmalloc_internal_pvalloc)(std::size_t size);

void  (*ldmalloc_internal_free)(void* ptr);


// Initialize the backup functions

// A mutex to allow only one thread to initialize ldmalloc
static std::mutex init_mutex;
// Flags the current thread as "initializing" if set
thread_local static bool is_this_thread_initializing;
// Whether ldmalloc still needs to be initialized
static bool init_required = true;
// Whether this thread still needs to be initialized
thread_local static bool this_thread_init_required = true;
// Flags ldmalloc as "finalizing" if set
static bool is_finalizing = false;

static void this_thread_init()
{
    // Initialize thread-local logging
    ldmalloc_log_thread_init();
    this_thread_init_required = false;
}

static void finalize()
{
    // Cleanup: disable memory allocation and unwind the logger
    is_finalizing = true;
    ldmalloc_log_finalize();
}

static void abort_handler(int /* signal */)
{
    std::signal(SIGABRT, SIG_DFL); // Avoid endless loop
    finalize();   // Clean up
    std::abort(); // Dump core
}

static void initialize()
{
    // Synchronize between threads so that initialize is called at most once,
    // but no thread may proceed before initialize has completed at least once
    if (is_this_thread_initializing)
    {
        fprintf(stderr, "Recursion detected in initialization of ldmalloc\n");
        std::abort();
    }
    std::lock_guard<std::mutex> lock(init_mutex);
    if (!init_required)
    {
        this_thread_init();
        return;
    }
    is_this_thread_initializing = true;

    // Set base versions
    ldmalloc_internal_malloc  = (void* (*)(std::size_t))              dlsym(RTLD_NEXT, "malloc");
    ldmalloc_internal_calloc  = (void* (*)(std::size_t, std::size_t)) dlsym(RTLD_NEXT, "calloc");
    ldmalloc_internal_realloc = (void* (*)(void*, std::size_t))       dlsym(RTLD_NEXT, "realloc");

    ldmalloc_internal_posix_memalign = (int (*)(void**, std::size_t, std::size_t)) dlsym(RTLD_NEXT, "posix_memalign");
    ldmalloc_internal_memalign       =       (void* (*)(std::size_t, std::size_t)) dlsym(RTLD_NEXT, "memalign");
    ldmalloc_internal_aligned_alloc  =       (void* (*)(std::size_t, std::size_t)) dlsym(RTLD_NEXT, "aligned_alloc");
    ldmalloc_internal_valloc         =                    (void* (*)(std::size_t)) dlsym(RTLD_NEXT, "valloc");
    ldmalloc_internal_pvalloc        =                    (void* (*)(std::size_t)) dlsym(RTLD_NEXT, "pvalloc");

    ldmalloc_internal_free = (void (*)(void*)) dlsym(RTLD_NEXT, "free");

    // Internal versions are now ready to use
    is_this_thread_initializing = false;

    // Install at_exit cleanup hook and signal handler
    std::atexit(finalize);
    std::signal(SIGABRT, abort_handler);

    // Initialize logging
    ldmalloc_log_init();
    this_thread_init();

    // Done initializing
    init_required = false;
}


// When a thread is initializing, use this buffer to allocate memory (instead of
// recursively calling initialize to fill ldmalloc_internal_*). This does not need
// to be atomic, because only one thread will actually ever use the buffer.
static char initialization_buffer[LDMALLOC_INITIALIZATION_BUFFER];
static std::ptrdiff_t initialization_buffer_position;

static void *buffer_alloc(std::size_t size, const char *name)
{
    // In some cases, dlsym (or other functions called by initialize) internally
    // allocate memory. If this thread is currently initializing ldmalloc (and
    // already calling that function), this would lead to an endless recursive
    // attempt at initializing ldmalloc (which fails for obvious reasons).
    // Instead, we use a temporary piece of memory allocated specifically for
    // this purpose.
    if (initialization_buffer_position + size > LDMALLOC_INITIALIZATION_BUFFER)
    {
        fprintf(stderr, "ldmalloc: allocation (%s) during initialization requested too much memory (%lu bytes), try increasing LDMALLOC_INITIALIZATION_BUFFER\n", name, size);
        return nullptr;
    }

    // Get the pointer, update the buffer position
    void* result = initialization_buffer + initialization_buffer_position;
    initialization_buffer_position += size;

    // Log and return
    fprintf(stderr, "ldmalloc: allocation (%s) during initialization: %lu bytes at position %p\n", name, size, result);
    return result;
}

// If a function requests memory in a manner not supported by buffer_alloc (e.g.
// aligned memory), abort.
static void buffer_abort(const char *name)
{
    fprintf(stderr, "ldmalloc: forbidden call to %s during initialization or finalization\n", name);
    std::abort();
}

// When a pointer returned by buffer_alloc is freed, log this, but perform no
// further actions.
static void buffer_free(void *ptr, const char *name)
{
    fprintf(stderr, "ldmalloc: freeing (%s) allocation at %p\n", name, ptr);
}


// Exported functions
// Those all call back into the base versions and log their parameters and result

extern "C" void* malloc(std::size_t size) LDMALLOC_NOEXCEPT
{
    if (is_this_thread_initializing) return buffer_alloc(size, "malloc");
    if (is_finalizing) buffer_abort("malloc");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    void* result = ldmalloc_internal_malloc(size);
    ldmalloc_log_malloc(size, result);
    return result;
}
extern "C" void* calloc(std::size_t count, std::size_t size) LDMALLOC_NOEXCEPT
{
    if (is_this_thread_initializing) return buffer_alloc(count * size, "calloc");
    if (is_finalizing) buffer_abort("calloc");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    void* result = ldmalloc_internal_calloc(count, size);
    ldmalloc_log_calloc(count, size, result);
    return result;
}
extern "C" void* realloc(void* ptr, std::size_t size) LDMALLOC_NOEXCEPT
{
    if (is_this_thread_initializing) buffer_abort("realloc");
    if (is_finalizing) buffer_abort("realloc");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    void* result = ldmalloc_internal_realloc(ptr, size);
    ldmalloc_log_realloc(ptr, size, result);
    return result;
}

extern "C" int posix_memalign(void** memptr, std::size_t alignment, std::size_t size) LDMALLOC_NOEXCEPT
{
    if (is_this_thread_initializing) buffer_abort("posix_memalign");
    if (is_finalizing) buffer_abort("posix_memalign");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    int result = ldmalloc_internal_posix_memalign(memptr, alignment, size);
    ldmalloc_log_posix_memalign(alignment, size, *memptr);
    return result;
}
extern "C" void* memalign(std::size_t alignment, std::size_t size)
{
    if (is_this_thread_initializing) buffer_abort("memalign");
    if (is_finalizing) buffer_abort("memalign");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    void* result = ldmalloc_internal_memalign(alignment, size);
    ldmalloc_log_memalign(alignment, size, result);
    return result;
}
extern "C" void* aligned_alloc(std::size_t alignment, std::size_t size) LDMALLOC_NOEXCEPT
{
    if (is_this_thread_initializing) buffer_abort("aligned_alloc");
    if (is_finalizing) buffer_abort("aligned_alloc");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    void* result = ldmalloc_internal_aligned_alloc(alignment, size);
    ldmalloc_log_aligned_alloc(alignment, size, result);
    return result;
}
extern "C" void* valloc(std::size_t size) LDMALLOC_NOEXCEPT
{
    if (is_this_thread_initializing) buffer_abort("valloc");
    if (is_finalizing) buffer_abort("valloc");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    void* result = ldmalloc_internal_valloc(size);
    ldmalloc_log_valloc(size, result);
    return result;
}
extern "C" void* pvalloc(std::size_t size)
{
    if (is_this_thread_initializing) buffer_abort("pvalloc");
    if (is_finalizing) buffer_abort("pvalloc");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    void* result = ldmalloc_internal_pvalloc(size);
    ldmalloc_log_pvalloc(size, result);
    return result;
}

extern "C" void free(void* ptr) LDMALLOC_NOEXCEPT
{
    // Check if the memory we are trying to free is within the initialization
    // buffer. If so, don't actually free it.
    if (ptr >= initialization_buffer && ptr < initialization_buffer + LDMALLOC_INITIALIZATION_BUFFER)
    {
        buffer_free(ptr, "free");
        return;
    }

    if (is_this_thread_initializing) buffer_abort("free");
    if (is_finalizing) buffer_abort("free");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    // Here, we output prior to freeing the memory, so that an implementation
    // of the logging interface may freely inspect the memory held by ptr.
    ldmalloc_log_free(ptr);
    ldmalloc_internal_free(ptr);
}

// Global operators new and delete

void* operator new(std::size_t size)
{
    if (is_this_thread_initializing) return buffer_alloc(size, "new");
    if (is_finalizing) buffer_abort("new");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    void* result = ldmalloc_internal_malloc(size);
    ldmalloc_log_new(size, result);
    return result;
}
void* operator new(std::size_t size, const std::nothrow_t&) LDMALLOC_NOEXCEPT
{
    if (is_this_thread_initializing) return buffer_alloc(size, "new(std::nothrow_t)");
    if (is_finalizing) buffer_abort("new(std::nothrow_t)");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    void* result = ldmalloc_internal_malloc(size);
    ldmalloc_log_new_nothrow(size, result);
    return result;
}
void* operator new[](std::size_t size)
{
    if (is_this_thread_initializing) return buffer_alloc(size, "new[]");
    if (is_finalizing) buffer_abort("new[]");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    void* result = ldmalloc_internal_malloc(size);
    ldmalloc_log_new_array(size, result);
    return result;
}
void* operator new[](std::size_t size, const std::nothrow_t&) LDMALLOC_NOEXCEPT
{
    if (is_this_thread_initializing) return buffer_alloc(size, "new[](std::nothrow_t)");
    if (is_finalizing) buffer_abort("new[](std::nothrow_t)");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    void* result = ldmalloc_internal_malloc(size);
    ldmalloc_log_new_nothrow_array(size, result);
    return result;
}

void operator delete(void* ptr) LDMALLOC_NOEXCEPT
{
    // Check if the memory we are trying to free is within the initialization
    // buffer. If so, don't actually free it.
    if (ptr >= initialization_buffer && ptr < initialization_buffer + LDMALLOC_INITIALIZATION_BUFFER)
    {
        buffer_free(ptr, "delete");
        return;
    }

    if (is_this_thread_initializing) buffer_abort("delete");
    if (is_finalizing) buffer_abort("delete");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    ldmalloc_log_delete(ptr);
    ldmalloc_internal_free(ptr);
}
void operator delete[](void* ptr) LDMALLOC_NOEXCEPT
{
    // Check if the memory we are trying to free is within the initialization
    // buffer. If so, don't actually free it.
    if (ptr >= initialization_buffer && ptr < initialization_buffer + LDMALLOC_INITIALIZATION_BUFFER)
    {
        buffer_free(ptr, "delete[]");
        return;
    }

    if (is_this_thread_initializing) buffer_abort("delete[]");
    if (is_finalizing) buffer_abort("delete[]");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    ldmalloc_log_delete_array(ptr);
    ldmalloc_internal_free(ptr);
}
void operator delete(void* ptr, const std::nothrow_t&) LDMALLOC_NOEXCEPT
{
    // Check if the memory we are trying to free is within the initialization
    // buffer. If so, don't actually free it.
    if (ptr >= initialization_buffer && ptr < initialization_buffer + LDMALLOC_INITIALIZATION_BUFFER)
    {
        buffer_free(ptr, "delete(std::nothrow_t)");
        return;
    }

    if (is_this_thread_initializing) buffer_abort("delete(std::nothrow_t)");
    if (is_finalizing) buffer_abort("delete(std::nothrow_t)");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    ldmalloc_log_delete_nothrow(ptr);
    ldmalloc_internal_free(ptr);
}
void operator delete[](void* ptr, const std::nothrow_t&) LDMALLOC_NOEXCEPT
{
    // Check if the memory we are trying to free is within the initialization
    // buffer. If so, don't actually free it.
    if (ptr >= initialization_buffer && ptr < initialization_buffer + LDMALLOC_INITIALIZATION_BUFFER)
    {
        buffer_free(ptr, "delete[](std::nothrow_t)");
        return;
    }

    if (is_this_thread_initializing) buffer_abort("delete[](std::nothrow_t)");
    if (is_finalizing) buffer_abort("delete[](std::nothrow_t)");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    ldmalloc_log_delete_nothrow_array(ptr);
    ldmalloc_internal_free(ptr);
}
void operator delete(void* ptr, std::size_t size)
{
    // Check if the memory we are trying to free is within the initialization
    // buffer. If so, don't actually free it.
    if (ptr >= initialization_buffer && ptr < initialization_buffer + LDMALLOC_INITIALIZATION_BUFFER)
    {
        buffer_free(ptr, "delete(std::size_t)");
        return;
    }

    if (is_this_thread_initializing) buffer_abort("delete(std::size_t)");
    if (is_finalizing) buffer_abort("delete(std::size_t)");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    ldmalloc_log_sized_delete(ptr, size);
    ldmalloc_internal_free(ptr);
}
void operator delete[](void* ptr, std::size_t size)
{
    // Check if the memory we are trying to free is within the initialization
    // buffer. If so, don't actually free it.
    if (ptr >= initialization_buffer && ptr < initialization_buffer + LDMALLOC_INITIALIZATION_BUFFER)
    {
        buffer_free(ptr, "delete[](std::size_t)");
        return;
    }

    if (is_this_thread_initializing) buffer_abort("delete[](std::size_t)");
    if (is_finalizing) buffer_abort("delete[](std::size_t)");
    if (init_required) initialize();
    if (this_thread_init_required) this_thread_init();

    ldmalloc_log_sized_delete_array(ptr, size);
    ldmalloc_internal_free(ptr);
}
