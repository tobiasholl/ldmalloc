// Copyright (C) 2017 Tobias Holl
// This file is subject to the license terms in the LICENSE file
// found in the top-level directory of this distribution

#include <cstddef>
#include <functional>
#include <iterator>
#include <memory>
#include <type_traits>
#include <utility>

#ifndef LDMALLOC_AVL_TREE_HPP
#define LDMALLOC_AVL_TREE_HPP

// Anonymous namespace for internal linkage
namespace
{

// Detect whether a particular type T is a std::pair<K, ...>
template <typename T, typename K>
struct is_pair_with_key : std::false_type {};

template <typename A, typename B>
struct is_pair_with_key<std::pair<A, B>, A> : std::true_type {};

// Compare the first item of a pair
template <typename Compare>
struct first_item_compare
{
    Compare compare;

    template <typename A, typename B>
    bool operator()(const A& a, const B& b)
    {
        return compare(a.first, b.first);
    }
};

// Detect if we are using first_item_compare
template <typename Compare>
struct is_pair_comparison : std::false_type {};

template <typename Compare>
struct is_pair_comparison<first_item_compare<Compare>> : std::true_type {};

} // namespace <anonymous>

namespace ldmalloc
{

// A node of the AVL tree
template <typename T>
struct avl_node
{
    T data;
    avl_node<T> *left;
    avl_node<T> *right;
    avl_node<T> *parent;
    std::intmax_t height;

    void recalculate_height()
    {
        if (left != nullptr && right != nullptr)
            height = std::max(left->height, right->height) + 1;
        else if (left != nullptr)
            height = left->height + 1;
        else if (right != nullptr)
            height = right->height + 1;
        else
            height = 0;
    }

    std::intmax_t balance()
    {
        if (left != nullptr && right != nullptr)
            return left->height - right->height;
        else if (left != nullptr)
            return left->height + 1;
        else if (right != nullptr)
            return - right->height - 1;
        else
            return 0;
    }
};

template <typename T>
class avl_iterator : std::iterator<std::bidirectional_iterator_tag, T>
{
public:
    // Create an iterator
    avl_iterator(avl_node<T> *pointer=nullptr) : m_pointer(pointer) {}

    // Move to the next node, in-order.
    avl_iterator<T>& operator++()
    {
        // If we have reached the end, stop moving.
        if (m_pointer == nullptr) return *this;

        // If we have reached the bottom of the tree, or otherwise need to move upwards
        if (m_pointer->right == nullptr)
        {
            // We have reached the bottom. All left children in the path upwards have already been visited, so we move upwards as long as we are the right child.
            while (m_pointer->parent != nullptr && m_pointer->parent->right == m_pointer) m_pointer = m_pointer->parent;

            // Now, we are the left child of a node, or the root node.
            // Then, move up one step further.
            m_pointer = m_pointer->parent;
        }
        else
        {
            // Otherwise, we are not done in the current branch.
            // Move down to the right child, and then as far left as possible
            m_pointer = m_pointer->right;
            while (m_pointer->left != nullptr) m_pointer = m_pointer->left;
        }

        return *this;
    }

    // Move to the previous node, in-order
    avl_iterator<T>& operator--()
    {
        // If we have reached the end, stop moving.
        if (m_pointer == nullptr) return *this;

        // If we have reached the bottom of the tree, or otherwise need to move upwards
        if (m_pointer->left == nullptr)
        {
            // We have reached the bottom. All right children in the path upwards have already been visited, so we move upwards as long as we are the left child.
            while (m_pointer->parent != nullptr && m_pointer->parent->left == m_pointer) m_pointer = m_pointer->parent;

            // Now, we are the right child of a node, or the root node.
            // Then, move up one step further.
            m_pointer = m_pointer->parent;
        }
        else
        {
            // Otherwise, we are not done in the current branch.
            // Move down to the left child, and then as far right as possible
            m_pointer = m_pointer->left;
            while (m_pointer->right != nullptr) m_pointer = m_pointer->right;
        }

        return *this;
    }

    avl_iterator<T> operator++(int)
    {
        avl_iterator<T> copy = *this;
        ++(*this);
        return copy;
    }

    avl_iterator<T> operator--(int)
    {
        avl_iterator<T> copy = *this;
        --(*this);
        return copy;
    }

    // Dereference the iterator
    T& operator*() { return m_pointer->data; }
    T* operator->() { return &(m_pointer->data); }

    avl_node<T>* raw() { return m_pointer; }

    // Comparisons
    bool operator==(const avl_iterator<T>& other) { return m_pointer == other.m_pointer; }
    bool operator!=(const avl_iterator<T>& other) { return m_pointer != other.m_pointer; }
private:
    avl_node<T> *m_pointer;
};

// An AVL tree
template <typename T, typename Compare=std::less<T>, typename Allocator=std::allocator<avl_node<T>>>
class avl_tree
{
public:
    avl_node<T> *allocate_node(const T& value)
    {
        return new (m_allocator.allocate(1)) avl_node<T> {value, nullptr, nullptr, nullptr, 0};
    }

    avl_node<T> *allocate_node(T&& value)
    {
        return new (m_allocator.allocate(1)) avl_node<T> {std::move(value), nullptr, nullptr, nullptr, 0};
    }

    void free_node(avl_node<T> *target)
    {
        target->~avl_node<T>();
        m_allocator.deallocate(target, 1);
    }

    avl_tree(const Compare& compare = Compare(), const Allocator& alloc = Allocator())
        : m_compare(compare)
        , m_allocator(alloc)
    {}

    avl_tree(const avl_tree<T, Compare, Allocator>& other) = delete;
    avl_tree(avl_tree<T, Compare, Allocator>&& other)
        : m_root(std::move(other.m_root))
        , m_size(std::move(other.m_size))
        , m_compare(std::move(other.m_compare))
        , m_allocator(std::move(other.m_allocator))
    {
        other.m_root = nullptr;
        other.m_size = 0;
    }

    avl_tree<T, Compare, Allocator>& operator=(const avl_tree<T, Compare, Allocator>& other) = delete;
    avl_tree<T, Compare, Allocator>& operator=(avl_tree<T, Compare, Allocator>&& other)
    {
        iterate([this](avl_node<T> *pointer){ free_node(pointer); });

        m_root = std::move(other.m_root);
        m_size = std::move(other.m_size);
        m_compare = std::move(other.m_compare);
        m_allocator = std::move(other.m_allocator);

        other.m_root = nullptr;
        other.m_size = 0;

        return *this;
    }

    ~avl_tree()
    {
        iterate([this](avl_node<T> *pointer){ free_node(pointer); });
    }

    avl_iterator<T> find(const T& value) const
    {
        avl_node<T> *iterator = m_root;
        while (true)
        {
            if (iterator == nullptr) return end();

            if      (m_compare(value, iterator->data)) iterator = iterator->left;
            else if (m_compare(iterator->data, value)) iterator = iterator->right;
            else return avl_iterator<T>(iterator);
        }
    }

    template <typename K>
    typename std::enable_if<is_pair_with_key<T, K>::value && is_pair_comparison<Compare>::value, avl_iterator<T>>::type find(const K& key) const
    {
        // This is for searches in maps.
        avl_node<T> *iterator = m_root;
        while (true)
        {
            if (iterator == nullptr) return end();

            if      (m_compare.compare(key, iterator->data.first)) iterator = iterator->left;
            else if (m_compare.compare(iterator->data.first, key)) iterator = iterator->right;
            else return avl_iterator<T>(iterator);
        }
    }

    std::size_t size() const { return m_size; }

    avl_iterator<T> begin() const
    {
        // Find the leftmost node in the tree
        avl_node<T> *leftmost = m_root;

        if (leftmost != nullptr)
            while (leftmost->left != nullptr)
                leftmost = leftmost->left;

        return avl_iterator<T>(leftmost);
    }

    avl_iterator<T> last() const
    {
        // Find the rightmost node in the tree
        avl_node<T> *rightmost = m_root;

        if (rightmost != nullptr)
            while (rightmost->right != nullptr)
                rightmost = rightmost->right;

        return avl_iterator<T>(rightmost);
    }

    avl_iterator<T> end() const { return avl_iterator<T>(nullptr); }

    template <typename OtherCompare>
    void merge(avl_tree<T, OtherCompare, Allocator>& other)
    {
        other.iterate(
            [this, &other](avl_node<T> *pointer)
            {
                this->insert(other.detach(pointer));
            }
        );
    }

    avl_iterator<T> insert(avl_node<T> *new_node)
    {
        // Set the root node if it doesn't exist yet
        if (m_root == nullptr)
        {
            m_root = new_node;
            new_node->parent = nullptr;
            ++m_size;
            return avl_iterator<T> { new_node };
        }

        // Otherwise, follow the tree to find the closest location
        avl_node<T> *iterator = m_root;
        while (true)
        {
            // Never attach nodes that are already present
            if (new_node == iterator) return end();

            // Follow the right branch
            if (m_compare(new_node->data, iterator->data))
            {
                // Try moving to the left subtree.
                if (iterator->left != nullptr)
                    iterator = iterator->left;
                else
                {
                    iterator->left = new_node;
                    new_node->parent = iterator;
                    break;
                }
            }
            else if (m_compare(iterator->data, new_node->data))
            {
                // Try moving to the right subtree.
                if (iterator->right != nullptr)
                    iterator = iterator->right;
                else
                {
                    iterator->right = new_node;
                    new_node->parent = iterator;
                    break;
                }
            }
            else
            {
                // A node with this value is already present.
                return end();
            }
        }

        // Then, rebalance
        iterator = new_node;
        for (iterator = new_node; iterator != nullptr; iterator = iterator->parent)
        {
            iterator->recalculate_height();
            rebalance(iterator);
        }

        // Increase size
        ++m_size;

        return avl_iterator<T> { new_node };
    }

    avl_iterator<T> insert(const T& value) { return insert(allocate_node(value)); }

    template <typename K>
    typename std::enable_if<is_pair_with_key<T, K>::value && is_pair_comparison<Compare>::value, avl_iterator<T>>::type insert(const K& key)
    {
        // This is for insertions in maps.
        T value(std::move(key), std::move(typename T::second_type()));
        return insert(allocate_node(std::move(value)));
    }

    avl_node<T> *detach(avl_iterator<T> iterator)
    {
        // Grab the node and its parent
        avl_node<T> *target = iterator.raw();
        avl_node<T> *parent = target->parent;

        // Handle the children
        if (target->left != nullptr && target->right != nullptr)
        {
            // Find successor
            avl_node<T> *successor;
            for (successor = target->right; successor->left != nullptr; successor = successor->left);

            // Swap with the target node
            using std::swap;
            swap(target->data, successor->data);

            // Detach the successor instead
            return detach(successor);
        }
        else if (target->left != nullptr)
        {
            target->left->parent = parent;

            if (parent == nullptr)
                m_root = target->left;
            else if (parent->left == target)
                parent->left = target->left;
            else
                parent->right = target->left;
        }
        else if (target->right != nullptr)
        {
            target->right->parent = parent;

            if (parent == nullptr)
                m_root = target->right;
            else if (parent->left == target)
                parent->left = target->right;
            else
                parent->right = target->right;
        }
        else
        {
            if (parent == nullptr)
                m_root = nullptr;
            else if (parent->left == target)
                parent->left = nullptr;
            else
                parent->right = nullptr;
        }

        // Clean up the target node
        target->parent = target->left = target->right = nullptr;
        target->height = 0;

        // Rebalance the tree
        for (; parent != nullptr; parent = parent->parent)
        {
            parent->recalculate_height();
            rebalance(parent);
        }

        // Reduce the size
        --m_size;

        return target;
    }

    avl_iterator<T> erase(avl_iterator<T> target)
    {
        auto next = target;
        ++next;

        free_node(detach(target));

        return next;
    }

    avl_node<T> *raw()
    {
        return m_root;
    }

private:
    avl_node<T> *m_root = nullptr;
    std::size_t m_size = 0;
    Compare m_compare;
    Allocator m_allocator;

    void rotate_left(avl_node<T> *current)
    {
        avl_node<T> *parent = current->parent;

        // Perform the rotation
        avl_node<T> *replacement = current->right;
        current->right = replacement->left;
        replacement->left = current;

        // Update parents
        replacement->parent = parent;
        current->parent = replacement;
        if (current->right != nullptr) current->right->parent = current;

        // Reattach the replacement node
        if (parent == nullptr)
            m_root = replacement;
        else if (parent->left == current)
            parent->left = replacement;
        else
            parent->right = replacement;

        // Recalculate heights
        current->recalculate_height();
        replacement->recalculate_height();
    }

    void rotate_right(avl_node<T> *current)
    {
        avl_node<T> *parent = current->parent;

        // Perform the rotation
        avl_node<T> *replacement = current->left;
        current->left = replacement->right;
        replacement->right = current;

        // Update parents
        replacement->parent = parent;
        current->parent = replacement;
        if (current->left != nullptr) current->left->parent = current;

        // Reattach the replacement node
        if (parent == nullptr)
            m_root = replacement;
        else if (parent->left == current)
            parent->left = replacement;
        else
            parent->right = replacement;

        // Recalculate heights
        current->recalculate_height();
        replacement->recalculate_height();
    }

    void rebalance(avl_node<T> *current)
    {
        auto balance = current->balance();
        if (balance > 1)
        {
            if (current->left != nullptr && current->left->balance() < 0)
                rotate_left(current->left);
            rotate_right(current);
        }
        else if (balance < -1)
        {
            if (current->right != nullptr && current->right->balance() > 0)
                rotate_right(current->right);
            rotate_left(current);
        }
    }

    void iterate(std::function<void(avl_node<T>*)> action)
    {
        iterate(action, m_root);
    }

    void iterate(std::function<void(avl_node<T>*)> action, avl_node<T> *current)
    {
        if (current == nullptr) return;
        iterate(action, current->left);
        iterate(action, current->right);
        action(current);
    }
};

// Imitated standard library types
template <typename K, typename V, typename Compare=std::less<K>, typename Allocator=std::allocator<avl_node<std::pair<K, V>>>>
using map = avl_tree<std::pair<K, V>, first_item_compare<Compare>, Allocator>;

} // namespace ldmalloc

#endif // LDMALLOC_AVL_TREE_HPP
