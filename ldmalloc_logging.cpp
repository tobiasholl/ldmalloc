// Copyright (C) 2016 Tobias Holl
// This file is subject to the license terms in the LICENSE file
// found in the top-level directory of this distribution.

#include <execinfo.h>
#include <unistd.h>

#include <atomic>
#include <chrono>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <functional>
#include <limits>

#include "ldmalloc_avl_tree.hpp"

// Pull in the base versions of calloc and free for the allocator

extern void* (*ldmalloc_internal_calloc)(std::size_t count, std::size_t size);
extern void  (*ldmalloc_internal_free)(void* ptr);

static thread_local bool ldmalloc_this_thread_disable_logging = false;

// Set up logging

#define LDMALLOC_LOG_LEVEL_ERROR   1
#define LDMALLOC_LOG_LEVEL_WARNING 2
#define LDMALLOC_LOG_LEVEL_VERBOSE 3
#define LDMALLOC_LOG_LEVEL_DEBUG   4

static unsigned int ldmalloc_loglevel = LDMALLOC_LOG_LEVEL_WARNING;

#ifndef LDMALLOC_NO_COLORS
#define LDMALLOC_COLOR_SGR(key) "\x1b[" key "m"
#else
#define LDMALLOC_COLOR_SGR(key)
#endif

#define LDMALLOC_MESSAGE_FATAL   LDMALLOC_COLOR_SGR("31;1") "["                          " FATAL "                           "] " LDMALLOC_COLOR_SGR("0")
#define LDMALLOC_MESSAGE_ERROR   LDMALLOC_COLOR_SGR("1")    "[" LDMALLOC_COLOR_SGR("31") " ERROR " LDMALLOC_COLOR_SGR("0;1") "] " LDMALLOC_COLOR_SGR("0")
#define LDMALLOC_MESSAGE_WARNING LDMALLOC_COLOR_SGR("1")    "[" LDMALLOC_COLOR_SGR("33") "WARNING" LDMALLOC_COLOR_SGR("0;1") "] " LDMALLOC_COLOR_SGR("0")
#define LDMALLOC_MESSAGE_VERBOSE LDMALLOC_COLOR_SGR("1")    "["                          "VERBOSE"                           "] " LDMALLOC_COLOR_SGR("0")
#define LDMALLOC_MESSAGE_DEBUG   LDMALLOC_COLOR_SGR("1")    "[" LDMALLOC_COLOR_SGR("32") " DEBUG " LDMALLOC_COLOR_SGR("0;1") "] " LDMALLOC_COLOR_SGR("0")

#define LDMALLOC_LOG_ALWAYS(...)    fprintf(stderr, __VA_ARGS__)
#define LDMALLOC_LOG(loglevel, ...) if (ldmalloc_loglevel >= loglevel) LDMALLOC_LOG_ALWAYS(__VA_ARGS__)

#define LDMALLOC_FATAL(...)   LDMALLOC_LOG_ALWAYS(LDMALLOC_MESSAGE_FATAL __VA_ARGS__);                                                      \
                              LDMALLOC_LOG_ALWAYS(LDMALLOC_MESSAGE_FATAL LDMALLOC_COLOR_SGR("31;1") "Aborting!\n" LDMALLOC_COLOR_SGR("0")); \
                              std::abort()

#define LDMALLOC_ERROR(...)   LDMALLOC_LOG(LDMALLOC_LOG_LEVEL_ERROR,   LDMALLOC_MESSAGE_ERROR   __VA_ARGS__)
#define LDMALLOC_WARNING(...) LDMALLOC_LOG(LDMALLOC_LOG_LEVEL_WARNING, LDMALLOC_MESSAGE_WARNING __VA_ARGS__)
#define LDMALLOC_VERBOSE(...) LDMALLOC_LOG(LDMALLOC_LOG_LEVEL_VERBOSE, LDMALLOC_MESSAGE_VERBOSE __VA_ARGS__)
#define LDMALLOC_DEBUG(...)   LDMALLOC_LOG(LDMALLOC_LOG_LEVEL_DEBUG,   LDMALLOC_MESSAGE_DEBUG   __VA_ARGS__)

// Array sizes. User-configurable sizes should be wrapped with #ifndef
#ifndef LDMALLOC_BACKTRACE_SIZE
#define LDMALLOC_BACKTRACE_SIZE 50
#endif

#define LDMALLOC_MAXIMUM_REFERRED_EVENTS 2


// Set up system clock for logging

using ldmalloc_clock = std::chrono::steady_clock;

// Set up thread support

#ifndef LDMALLOC_MAX_THREADS
#define LDMALLOC_MAX_THREADS 2000
#endif

// Unnamed namespace to force internal linkage
namespace
{

// Internal standard library compliant allocator (Allocator concept)
template <typename T>
struct ldmalloc_allocator
{
    using value_type = T;

    ldmalloc_allocator() {}
    template <typename U> ldmalloc_allocator(const ldmalloc_allocator<U>&) {}
    template <typename U> ldmalloc_allocator(ldmalloc_allocator<U>&&) {}

    T* allocate(std::size_t n, const void * = nullptr)
    {
        LDMALLOC_DEBUG("ldmalloc_allocator: requesting %lu %s of size %lu\n", n, (n == 1) ? "item" : "items", sizeof(T));
        T* ptr = (T*) ldmalloc_internal_calloc(n, sizeof(T));
        if (ptr == nullptr)
        {
            LDMALLOC_ERROR("ldmalloc: failed to allocate memory\n");
            std::abort();
        }
        return ptr;
    }

    void deallocate(T* ptr, std::size_t n)
    {
        LDMALLOC_DEBUG("ldmalloc_allocator: freeing %lu %s\n", n, (n == 1) ? "object" : "objects");
        ldmalloc_internal_free(ptr);
    }

    bool operator==(const ldmalloc_allocator<T>&) { return true; }
    bool operator!=(const ldmalloc_allocator<T>&) { return false; }
};

// Using-declarations to simplify use of this allocator
template <typename T, typename Compare = std::less<T>>
using ldmalloc_set = ldmalloc::avl_tree<T, Compare, ldmalloc_allocator<ldmalloc::avl_node<T>>>;

template <typename K, typename V, typename Compare = std::less<K>>
using ldmalloc_map = ldmalloc::map<K, V, Compare, ldmalloc_allocator<ldmalloc::avl_node<std::pair<K, V>>>>;

// Allocation tracking
struct stored_backtrace
{
    int size;
    void *frames[LDMALLOC_BACKTRACE_SIZE];

    stored_backtrace()
    {
        ldmalloc_this_thread_disable_logging = true;
        size = backtrace(frames, LDMALLOC_BACKTRACE_SIZE);
        ldmalloc_this_thread_disable_logging = false;
    }

    void write(int fd) const { backtrace_symbols_fd(frames, size, fd); }
};

struct tracking
{
    std::size_t size;                // The size of the allocation made, if any
    ldmalloc_clock::time_point time; // The time the event took place at
    stored_backtrace trace;          // The location at which the event happened
    std::size_t thread;              // The thread in which the event happened

    // The type of the event
    enum event_type { ALLOCATION, DEALLOCATION, INVALID };
    mutable event_type type;

    // Reference other events (e.g. matching allocation) here
    tracking *referred[LDMALLOC_MAXIMUM_REFERRED_EVENTS];

    tracking(event_type item_type, std::size_t thread_id, std::size_t allocation_size = 0)
        : size(allocation_size)
        , time(ldmalloc_clock::now())
        , trace()
        , thread(thread_id)
        , type(item_type)
    {
        std::fill_n(referred, LDMALLOC_MAXIMUM_REFERRED_EVENTS, nullptr);
    }

    bool operator<(const tracking& other) const { return time < other.time; }
};

// This map will store any memory allocated in this thread.
struct allocation_map
{
    ldmalloc_map<void*, ldmalloc_set<tracking>> events;

    // Dump a log of all leaked memory and of all unmatched deallocations
    void write() const
    {
        for (const auto& [key, set] : events)
        {
            for (const auto& entry : set)
            {
                switch (entry.type)
                {
                    case tracking::ALLOCATION:   LDMALLOC_ERROR("Leaking %lu bytes at %p, allocated in thread %lu at\n", entry.size, key, entry.thread);
                                                 entry.trace.write(STDERR_FILENO);
                                                 break;
                    case tracking::DEALLOCATION: LDMALLOC_ERROR("Freed memory at %p without matching allocation in thread %lu at\n", key, entry.thread);
                                                 entry.trace.write(STDERR_FILENO);
                                                 if (entry.referred[0] != nullptr)
                                                 {
                                                     LDMALLOC_ERROR("Last recorded allocation for %p (%lu bytes) in thread %lu at\n", key, entry.referred[0]->size, entry.referred[0]->thread);
                                                     entry.referred[0]->trace.write(STDERR_FILENO);
                                                 }
                                                 if (entry.referred[1] != nullptr)
                                                 {
                                                     LDMALLOC_ERROR("Memory was freed in thread %lu at\n", entry.referred[0]->thread);
                                                     entry.referred[1]->trace.write(STDERR_FILENO);
                                                 }
                                                 break;
                    case tracking::INVALID:      continue;
                }
            }
        }
    }

    // Add all data in the other allocation map to this allocation map
    void merge(allocation_map& other)
    {
        LDMALLOC_DEBUG("Merging allocation maps:\n");
        // Iterate over all map entries
        for (auto iterator = other.events.begin(); iterator != other.events.end();)
        {
            // Prevent iterator invalidation
            auto next = iterator;
            ++next;

            // Check whether the key already exists
            auto match = events.find(iterator->first);

            LDMALLOC_DEBUG(" - Entry at %p with %lu %s: %s\n", iterator->first, iterator->second.size(), (iterator->second.size() == 1) ? "event" : "events", (match != events.end()) ? "Merging" : "Stealing");

            // If so, merge the two sets, otherwise just steal the entire node
            if (match != events.end())
                match->second.merge(iterator->second);
            else
                events.insert(other.events.detach(iterator));

            // Advance the iterator
            iterator = next;
        }
    }

    // Remove all leaks that were freed in the other map, and all unmatched deallocations for which a matching allocation can be found
    void consolidate()
    {
        LDMALLOC_DEBUG("Consolidating %lu %s\n", events.size(), (events.size() == 1) ? "entry" : "entries");

        // Iterate over all overlapping keys, i.e. all pointers that have recorded allocations and deallocations
        // To do this, iterate over all keys in the list of events
        for (auto& [key, set] : events)
        {
            LDMALLOC_DEBUG(" - Entry at %p with %lu %s\n", key, set.size(), (set.size() == 1) ? "event" : "events");

            // Keep track of the allocation status and the previous event
            bool is_allocated = false;
            auto previous = set.end();

            // Keep track of the last allocation and matching deallocation
            auto last_allocation = set.end();
            auto first_deallocation = set.end();

            // Now make sure that the space is never allocated when allocating, and never unallocated when deallocating
            for (auto current = set.begin(); current != set.end(); ++current)
            {
                // If the allocation status doesn't fit, the only possible explanations are two sequential deallocations (double free) or two sequential allocations.
                // Otherwise, invalidate both this and the previous event
                switch (current->type)
                {
                    case tracking::ALLOCATION:
                        // We keep all allocations alive, and do nothing. The matching deallocation will mark them as invalid.
                        is_allocated = true;
                        last_allocation = current;
                        first_deallocation = set.end();
                        break;
                    case tracking::DEALLOCATION:
                        // An error occurs during deallocation if the space is not already allocated. Otherwise, invalidate the current and the previous items
                        if (is_allocated && previous != set.end())
                        {
                            previous->type = current->type = tracking::INVALID;
                            LDMALLOC_DEBUG("    - Removing allocation and deallocation of pointer %p (from thread %lu to thread %lu)\n", key, previous->thread, current->thread);

                            first_deallocation = current;
                        }
                        else
                        {
                            // Store the matching allocation in the referral pointer
                            if (last_allocation != set.end())
                                current->referred[0] = &(last_allocation.raw()->data);
                            if (first_deallocation != set.end())
                                current->referred[1] = &(first_deallocation.raw()->data);
                            LDMALLOC_DEBUG("    - Referring unmatched deallocation of pointer %p to previous allocation\n", key);
                        }

                        // Remove the current allocation
                        is_allocated = false;
                        break;
                    case tracking::INVALID:
                        // We shouldn't be encountering any invalid events here. If we do, something is wrong.
                        LDMALLOC_WARNING("Encountered invalid event in allocation_map::consolidate.\n");
                        continue;
                }

                previous = current;
            }
        }
    }
};


// Store the map in a buffer to ensure that it is not destroyed before all memory
// allocations have gone through

ldmalloc_clock::time_point initialization_time;

std::atomic<std::ptrdiff_t> allocation_map_offset;
char allocation_map_storage[sizeof(allocation_map) * LDMALLOC_MAX_THREADS];
bool allocation_map_active[LDMALLOC_MAX_THREADS];

std::ptrdiff_t last_valid_map_pointer = (LDMALLOC_MAX_THREADS - 1) * sizeof(allocation_map);

thread_local std::size_t thread_index = std::numeric_limits<std::size_t>::max();
thread_local allocation_map* allocations = nullptr;

// Get the allocation map for a given thread index
allocation_map& get_allocation_map(std::size_t thread_index)
{
    std::ptrdiff_t offset = thread_index * sizeof(allocation_map);
    return *reinterpret_cast<allocation_map*>(allocation_map_storage + offset);
}

// Destroy the allocation map for a given thread index
void destroy_allocation_map(std::size_t thread_index)
{
    std::ptrdiff_t offset = thread_index * sizeof(allocation_map);
    allocation_map *map = reinterpret_cast<allocation_map*>(allocation_map_storage + offset);
    map->~allocation_map();
    allocation_map_active[thread_index] = false;
}

// Tracking backend

void track(void* location, tracking&& args)
{
    if (location == nullptr) return;

    if (allocations == nullptr)
    {
        LDMALLOC_FATAL("Cannot track event in thread %lu. Try increasing LDMALLOC_MAX_THREADS (currently %d).\n", thread_index, LDMALLOC_MAX_THREADS);
        std::abort();
    }

    // Fetch the ordered set storing the events for this particular location
    auto iterator = allocations->events.find(location);
    if (iterator == allocations->events.end())
        iterator = allocations->events.insert(location);

    // Insert the new event
    auto new_event = iterator->second.insert(std::move(args));

    // If there are entries present, react to illegal deallocations by shutting down before libc does
    if (new_event->type == tracking::DEALLOCATION)
    {
        // Obtain the previous entry (or nullptr)
        auto previous = new_event;
        --previous;

        // React to illegal deallocations
        if (previous == iterator->second.end() || previous->type == tracking::DEALLOCATION)
        {
            LDMALLOC_FATAL("Illegal deallocation of pointer %p in thread %lu.\n", location, thread_index);
        }
    }
}

} // End of unnamed namespace


// Logging functions for C/POSIX

void ldmalloc_log_malloc(std::size_t size, void* result)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("malloc(size=%lu) => %p\n", size, result);
    track(result, tracking { tracking::ALLOCATION, thread_index, size });
}
void ldmalloc_log_calloc(std::size_t count, std::size_t size, void* result)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("calloc(count=%lu, size=%lu) => %p\n", count, size, result);
    track(result, tracking { tracking::ALLOCATION, thread_index, count * size });
}
void ldmalloc_log_realloc(void* ptr, std::size_t size, void* result)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("realloc(ptr=%p, size=%lu) => %p\n", ptr, size, result);
    track(ptr, tracking { tracking::DEALLOCATION, thread_index });
    track(result, tracking { tracking::ALLOCATION, thread_index, size });
}

void ldmalloc_log_posix_memalign(std::size_t alignment, std::size_t size, void* new_ptr)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("posix_memalign(alignment=%lu, size=%lu) => %p\n", alignment, size, new_ptr);
    track(new_ptr, tracking { tracking::ALLOCATION, thread_index, size });
}
void ldmalloc_log_memalign(std::size_t alignment, std::size_t size, void* result)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("memalign(alignment=%lu, size=%lu) => %p\n", alignment, size, result);
    track(result, tracking { tracking::ALLOCATION, thread_index, size });
}
void ldmalloc_log_aligned_alloc(std::size_t alignment, std::size_t size, void* result)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("aligned_alloc(alignment=%lu, size=%lu) => %p\n", alignment, size, result);
    track(result, tracking { tracking::ALLOCATION, thread_index, size });
}
void ldmalloc_log_valloc(std::size_t size, void* result)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("valloc(size=%lu) => %p\n", size, result);
    track(result, tracking { tracking::ALLOCATION, thread_index, size });
}
void ldmalloc_log_pvalloc(std::size_t size, void* result)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("pvalloc(size=%lu) => %p\n", size, result);
    track(result, tracking { tracking::ALLOCATION, thread_index, size });
}

void ldmalloc_log_free(void* ptr)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("free(ptr=%p)\n", ptr);
    track(ptr, tracking { tracking::DEALLOCATION, thread_index });
}


// Logging functions for C++

void ldmalloc_log_new(std::size_t size, void* result)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("new(size=%lu) => %p\n", size, result);
    track(result, tracking { tracking::ALLOCATION, thread_index, size });
}
void ldmalloc_log_new_array(std::size_t size, void* result)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("new[](size=%lu) => %p\n", size, result);
    track(result, tracking { tracking::ALLOCATION, thread_index, size });
}
void ldmalloc_log_new_nothrow(std::size_t size, void* result)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("new(nothrow, size=%lu) => %p\n", size, result);
    track(result, tracking { tracking::ALLOCATION, thread_index, size });
}
void ldmalloc_log_new_nothrow_array(std::size_t size, void* result)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("new[](nothrow, size=%lu) => %p\n", size, result);
    track(result, tracking { tracking::ALLOCATION, thread_index, size });
}

void ldmalloc_log_delete(void* ptr)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("delete(ptr=%p)\n", ptr);
    track(ptr, tracking { tracking::DEALLOCATION, thread_index });
}
void ldmalloc_log_delete_array(void* ptr)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("delete[](ptr=%p)\n", ptr);
    track(ptr, tracking { tracking::DEALLOCATION, thread_index });
}
void ldmalloc_log_delete_nothrow(void* ptr)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("delete(nothrow, ptr=%p)\n", ptr);
    track(ptr, tracking { tracking::DEALLOCATION, thread_index });
}
void ldmalloc_log_delete_nothrow_array(void* ptr)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("delete[](nothrow, ptr=%p)\n", ptr);
    track(ptr, tracking { tracking::DEALLOCATION, thread_index });
}
void ldmalloc_log_sized_delete(void* ptr, std::size_t size)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("delete(ptr=%p, size=%lu)\n", ptr, size);
    track(ptr, tracking { tracking::DEALLOCATION, thread_index });
}
void ldmalloc_log_sized_delete_array(void* ptr, std::size_t size)
{
    if (ldmalloc_this_thread_disable_logging) return;

    LDMALLOC_VERBOSE("delete[](ptr=%p, size=%lu)\n", ptr, size);
    track(ptr, tracking { tracking::DEALLOCATION, thread_index });
}



// Logging: init and finalize

// This function is only called once at program start.
void ldmalloc_log_init()
{
    LDMALLOC_DEBUG("Setting up ldmalloc logging...\n");

    // Grab log level
    const char* env_loglevel = std::getenv("LDMALLOC_LOGLEVEL");
    if (env_loglevel)
        ldmalloc_loglevel = std::atoi(env_loglevel);

    // Grab current time
    initialization_time = ldmalloc_clock::now();

    LDMALLOC_DEBUG("Set up ldmalloc logging with verbosity level %u.\n", ldmalloc_loglevel);
}

// This function is called within every thread to initialize the tracking
void ldmalloc_log_thread_init()
{
    LDMALLOC_DEBUG("Setting up thread-local ldmalloc logging...\n");

    // Grab space for the allocation map in this thread
    std::ptrdiff_t offset = allocation_map_offset.fetch_add(sizeof(allocation_map));
    thread_index = offset / sizeof(allocation_map);

    if (offset > last_valid_map_pointer)
    {
        LDMALLOC_FATAL("Could not allocate memory to track allocations in thread %lu. Try increasing LDMALLOC_MAX_THREADS (currently %d).\n", thread_index, LDMALLOC_MAX_THREADS);
    }

    // Initialize the allocation map for this thread
    allocations = new (allocation_map_storage + offset) allocation_map();

    // Activate the allocation map in this thread
    allocation_map_active[thread_index] = true;

    LDMALLOC_DEBUG("Set up thread-local ldmalloc logging for thread index %lu.\n", thread_index);
}

// This function should be called once at cleanup
void ldmalloc_log_finalize()
{
    LDMALLOC_DEBUG("Finalizing ldmalloc logging...\n");

    allocation_map summary;

    // Merge all thread diagnostics to eliminate allocations that took place across thread boundaries
    for (std::size_t index = 0; index < LDMALLOC_MAX_THREADS; ++index)
        if (allocation_map_active[index])
            summary.merge(get_allocation_map(index));

    summary.consolidate();
    summary.write();

    // Destroy all allocation maps
    for (std::size_t index = 0; index < LDMALLOC_MAX_THREADS; ++index)
        if (allocation_map_active[index])
            destroy_allocation_map(index);
}
