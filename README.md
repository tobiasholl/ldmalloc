# ldmalloc

`ldmalloc` is a runtime analysis tool that detects memory errors by tracing all allocations and deallocations.

### Usage

To use `ldmalloc`, you can either link your program directly to the library, or you can preload the library using the `LD_PRELOAD` environment variable:

    LD_PRELOAD=./ldmalloc.so ./your_target_binary


### Output

`ldmalloc` reports every leaked allocation and every allocation that was deallocated multiple times (*double free*) at the end of the target program's execution:

    $ LD_PRELOAD=./ldmalloc.so ./memory_leak
    [ ERROR ] Leaking 72704 bytes at 0x21bf010, allocated at
    ./ldmalloc.so(_Z19ldmalloc_log_mallocmPv+0x77)[0x7f180b4e0a47]
    ./ldmalloc.so(malloc+0x67)[0x7f180b4d5137]
    /usr/lib/libstdc++.so.6(+0x8ba30)[0x7f180afb9a30]
    /lib64/ld-linux-x86-64.so.2(+0xf4fa)[0x7f180b7044fa]
    /lib64/ld-linux-x86-64.so.2(+0xf60b)[0x7f180b70460b]
    /lib64/ld-linux-x86-64.so.2(+0xdaa)[0x7f180b6f5daa]
    [ ERROR ] Leaking 4096 bytes at 0x21d0e90, allocated at
    ./ldmalloc.so(_Z22ldmalloc_log_new_arraymPv+0x77)[0x7f180b4e1447]
    ./ldmalloc.so(_Znam+0x67)[0x7f180b4d5af7]
    ./memory_leak[0x40144b]
    ./memory_leak[0x401189]
    /usr/lib/libc.so.6(__libc_start_main+0xf1)[0x7f180a695291]
    ./memory_leak[0x4012aa]


Occasionally, libraries may allocate memory designed to only be freed by the operating system after execution. Here, the first memory "leak" is caused by `libdl` - the memory is allocated when the program's dynamic library dependencies are loaded. `libpthread` is another frequent offender. However, you can easily separate errors caused from within your program from those caused by other libraries - just look at the backtrace. To translate the memory addresses into function or file names and line numbers, use the `addr2line` utility included with most distributions.

### Parameters

You can increase or decrease the output verbosity by setting the `LDMALLOC_LOGLEVEL` environment variable. If the variable is not set, the value defaults to `2`:

| `LDMALLOC_LOGLEVEL` | Verbosity |
| ------------------- | --------- |
| `0`                 | No output |
| `1`                 | Errors    |
| `2`                 | Warnings  |
| `3`                 | Trace     |
| `4`                 | Debug     |

### Compiling

`ldmalloc` is written in C++17, which means that a recent C++ compiler is required (e.g. `gcc` ≥ 7.0). If your terminal does not support colors, you can disable them by adding the `-DLDMALLOC_NO_COLORS` flag. Simply run `make` to build `ldmalloc`.

### License

This library is released under version 3 of the GNU Lesser General Public License, as outlined in the [`LICENSE`](https://gitlab.com/tobiasholl/ldmalloc/blob/master/LICENSE) file in the top-level directory of this repository.

